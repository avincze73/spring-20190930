package hu.sprint.nn.project.client.rest;

import com.netflix.discovery.converters.Auto;
import hu.sprint.nn.entity.Department;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class DepartmentRestService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ProjectServiceClient projectServiceClient;


    @GetMapping("/api/getonedepartment")
    public ResponseEntity<?> getOneDepartment(){

        HttpEntity<String> header = new HttpEntity<>(getHeaders());

        Department department
                = restTemplate.exchange("http://localhost:8081/api/get/103",
                HttpMethod.GET, header, Department.class).getBody();
        return new ResponseEntity<>(department, HttpStatus.OK);


    }

    @GetMapping("/api/getonedepartment2")
    public ResponseEntity<?> getOneDepartment2(){

        Department department = projectServiceClient
                .getDepartment("102").getBody();
        System.out.println(department.getName());
        return new ResponseEntity<>(department, HttpStatus.OK);

    }


    private static HttpHeaders getHeaders() {
        String plainCredentials = "admin:admin";
        String base64Credentials =
                new String(Base64.encodeBase64(plainCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

}
