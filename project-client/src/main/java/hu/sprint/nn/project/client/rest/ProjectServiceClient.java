package hu.sprint.nn.project.client.rest;

import hu.sprint.nn.entity.Department;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("project-service")
public interface ProjectServiceClient {
    @GetMapping("/api/get/{id}")
    ResponseEntity<Department> getDepartment(@PathVariable("id") String id);
}
