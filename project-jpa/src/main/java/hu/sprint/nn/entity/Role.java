package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Role")
@Getter
@Setter
@NoArgsConstructor
public class Role extends NNEntity {

    @Column(name = "name")
    private String name;

    public Role(String name) {
        this.name = name;
    }
}
