package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Department")
@Getter
@Setter
public class Department extends NNEntity {

    @Column(name = "name")
    private String name;
}
