package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
public class Address {

    @Column(name = "zip")
    private Integer zip;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    public Address(Integer zip, String city, String street) {
        this.zip = zip;
        this.city = city;
        this.street = street;
    }
}
