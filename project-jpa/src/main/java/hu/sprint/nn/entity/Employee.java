package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Component
@Entity
@Table(name = "Employee")
public class Employee extends NNEntity {

    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "loginName")
    private String loginName;
    @Column(name = "password")
    private String password;
    @Column(name = "title")
    private String title;
    @Column(name = "salary")
    private int salary;

    @ManyToOne
    @Basic(optional = true)
    @JoinColumn(name = "bossId", referencedColumnName = "id")
    private Employee boss;

    @Embedded
    private Address address;

    @ManyToOne
    @JoinColumn(name = "departmentId", referencedColumnName = "id")
    private Department department;

    @ManyToMany
    @JoinTable(name = "EmployeesRoles",
            joinColumns = @JoinColumn(name = "employeeId", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "roleId", referencedColumnName = "id"))
    private List<Role> roleList;

}