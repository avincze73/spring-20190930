package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Project")
@Getter
@Setter
public class Project extends NNEntity {

    @Column(name = "name")
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "fromDate")
    private Date fromDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "toDate")
    private Date toDate;

    @ManyToOne
    @JoinColumn(name = "ownerId", referencedColumnName = "id")
    private Employee owner;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "projectId", referencedColumnName = "id")
    private List<Task> taskList;



}
