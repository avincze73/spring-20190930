package hu.sprint.nn.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Task")
@Getter
@Setter
public class Task extends NNEntity {

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "participantId", referencedColumnName = "id")
    private Employee participant;

}
