package hu.sprint.nn;

import hu.sprint.nn.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        EntityManagerFactory factory =
                Persistence.createEntityManagerFactory("project-jpa-pu");
        EntityManager em = factory.createEntityManager();

        em.getTransaction().begin();
        Role role1 = new Role();
        role1.setName("admin");
        em.persist(role1);

        Role  role2 = new Role();
        role2.setName("user");
        em.persist(role2);

        Department department1 = new Department();
        department1.setName("IT");
        em.persist(department1);

        Department department2 = new Department();
        department2.setName("HR");
        em.persist(department2);


        Employee employee1 = new Employee();
        employee1.setFirstName("firstName1");
        employee1.setLastName("lastName1");
        employee1.setTitle("title1");
        employee1.setLoginName("loginName1");
        employee1.setPassword("password1");
        employee1.setSalary(100);
        employee1.setAddress(new Address(1111, "city1", "street1"));
        employee1.setRoleList(new ArrayList<Role>());
        employee1.getRoleList().add(role1);
        employee1.setDepartment(department1);
        em.persist(employee1);


        Employee employee2 = new Employee();
        employee2.setBoss(employee1);
        employee2.setFirstName("firstName2");
        employee2.setLastName("lastName2");
        employee2.setTitle("title2");
        employee2.setLoginName("loginName2");
        employee2.setPassword("password2");
        employee2.setSalary(200);
        employee2.setAddress(new Address(222, "city2", "street2"));
        employee2.setRoleList(new ArrayList<Role>());
        employee2.getRoleList().add(role2);
        employee2.setDepartment(department2);
        em.persist(employee2);

        Project project = new Project();
        project.setName("project1");
        project.setFromDate(new Date());
        project.setToDate(new Date());
        project.setOwner(employee1);
        project.setTaskList(new ArrayList<Task>());
        Task task = new Task();
        task.setName("task1");
        task.setParticipant(employee2);
        project.getTaskList().add(task);
        em.persist(project);
        em.getTransaction().commit();
    }
}
