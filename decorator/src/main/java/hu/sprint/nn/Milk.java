package hu.sprint.nn;

public class Milk extends Extra {
    public Milk(Product product) {
        super(product);
    }

    @Override
    public int price() {
        return super.price() + 20;
    }
}
