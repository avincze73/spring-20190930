package hu.sprint.nn;

public class Sugar extends Extra {
    public Sugar(Product product) {
        super(product);
    }

    @Override
    public int price() {
        return super.price() + 10;
    }
}
