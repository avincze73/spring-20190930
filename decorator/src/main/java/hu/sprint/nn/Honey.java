package hu.sprint.nn;

public class Honey extends Extra {
    public Honey(Product product) {
        super(product);
    }

    @Override
    public int price() {
        return super.price() + 30;
    }
}
