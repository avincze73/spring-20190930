package hu.sprint.nn;

public abstract class Extra implements Product {

    private Product product;

    @Override
    public int price() {
        return product.price();
    }

    public Extra(Product product) {
        this.product = product;
    }
}
