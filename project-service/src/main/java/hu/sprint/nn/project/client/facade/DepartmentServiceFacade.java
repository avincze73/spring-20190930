package hu.sprint.nn.project.client.facade;

import hu.sprint.nn.entity.Department;
import hu.sprint.nn.project.client.message.GetAllDepartmentsResponse;
import hu.sprint.nn.project.client.message.SaveDepartmentRequest;
import hu.sprint.nn.project.client.message.SaveDepartmentResponse;
import hu.sprint.nn.project.client.session.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceFacade {

    @Autowired
    private DepartmentService departmentService;

    public SaveDepartmentResponse save(SaveDepartmentRequest request){
        SaveDepartmentResponse response = new SaveDepartmentResponse();
        try {
            Department department = new Department();
            department.setName(request.getDepartmentName());
            departmentService.save(department);
            response.setResult("OK");
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setResult("ERROR");
        }
        return response;
    }

    public GetAllDepartmentsResponse getAll(){
        GetAllDepartmentsResponse response = new GetAllDepartmentsResponse();
        try {
            response.getDepartmentList().addAll(departmentService.getAll());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response;
    }

}
