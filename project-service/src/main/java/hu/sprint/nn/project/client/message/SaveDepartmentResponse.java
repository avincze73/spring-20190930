package hu.sprint.nn.project.client.message;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SaveDepartmentResponse implements Serializable {

    private String result;

    public SaveDepartmentResponse(String departmentName) {
        this.result = result;
    }
}
