package hu.sprint.nn.project.client.message;

import hu.sprint.nn.entity.Department;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GetAllDepartmentsResponse implements Serializable {

    private String result;
    List<Department> departmentList;

    public GetAllDepartmentsResponse() {
        this.result = result;
        this.departmentList = new ArrayList<>();
    }
}
