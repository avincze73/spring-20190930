package hu.sprint.nn.project.client.runner;

import hu.sprint.nn.project.client.facade.DepartmentServiceFacade;
import hu.sprint.nn.project.client.message.GetAllDepartmentsResponse;
import hu.sprint.nn.project.client.message.SaveDepartmentRequest;
import hu.sprint.nn.project.client.message.SaveDepartmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class DepartmentRunner implements CommandLineRunner {
    @Autowired
    //@Qualifier("")
    private DepartmentServiceFacade departmentServiceFacade;

    @Override
    public void run(String... args) throws Exception {
        SaveDepartmentRequest saveDepartmentRequest = new SaveDepartmentRequest();
        saveDepartmentRequest.setDepartmentName("department1");
        SaveDepartmentResponse departmentResponse =
                departmentServiceFacade.save(saveDepartmentRequest);

        saveDepartmentRequest = new SaveDepartmentRequest();
        saveDepartmentRequest.setDepartmentName("department2");
        departmentResponse =
                departmentServiceFacade.save(saveDepartmentRequest);

        GetAllDepartmentsResponse getAllDepartmentsResponse =
                departmentServiceFacade.getAll();
        getAllDepartmentsResponse.getDepartmentList()
                .forEach(department -> System.out.println(department.getName()));
    }
}
