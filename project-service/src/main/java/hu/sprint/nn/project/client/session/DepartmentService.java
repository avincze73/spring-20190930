package hu.sprint.nn.project.client.session;

import hu.sprint.nn.entity.Department;
import hu.sprint.nn.project.client.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    @Autowired
    private DepartmentRepository repository;

    public List<Department> getAll(){
        return repository.findAll();
    }

    public Department getOne(Integer id){
        return repository.getOne(id);
    }
    public void deleteOne(Integer id){
        repository.deleteById(id);
    }


    public void save(Department department){
        repository.save(department);
    }
}
