package hu.sprint.nn.project.client.rest;

import hu.sprint.nn.entity.Department;
import hu.sprint.nn.project.client.session.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DepartmentRestService {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/api/get")
    public ResponseEntity<?> getAll(){
        List<Department> departmentList = new ArrayList<>();
        departmentList.addAll(departmentService.getAll());
        if (departmentList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<List<Department>>(departmentList, HttpStatus.OK);
        }
    }

    @GetMapping("/api/get/{id}")
    public ResponseEntity<?> getOne(@PathVariable String id){
       Department department = departmentService.getOne(Integer.parseInt(id));
       return new ResponseEntity<Department>(department, HttpStatus.OK);
    }

    @DeleteMapping("/api/delete/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable String id){
        departmentService.deleteOne(Integer.parseInt(id));
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @PutMapping("/api/put/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable String id,
                                       @RequestBody Department department){
        Department oldDepartment = departmentService.getOne(Integer.parseInt(id));
        oldDepartment.setName(department.getName());
        departmentService.save(oldDepartment);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @PostMapping("/api/add")
    public ResponseEntity<?> save(@RequestBody Department department){
        Department newDepartment = new Department();
        newDepartment.setName(department.getName());
        departmentService.save(newDepartment);
        return new ResponseEntity<>( HttpStatus.OK);
    }
}
