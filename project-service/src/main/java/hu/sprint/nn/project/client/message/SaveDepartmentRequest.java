package hu.sprint.nn.project.client.message;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SaveDepartmentRequest implements Serializable {

    private String departmentName;

    public SaveDepartmentRequest(String departmentName) {
        this.departmentName = departmentName;
    }
}
