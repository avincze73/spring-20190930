package hu.sprint.nn.project.client.repository;

import hu.sprint.nn.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    List<Department> findByName(String name);
    Department findTopByNameOrderByIdDesc(String name);
}
