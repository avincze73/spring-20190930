package hu.sprint.nn;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App2
{
    public static void main( String[] args )
    {
        ApplicationContext context1 =
                new AnnotationConfigApplicationContext("hu.sprint.nn");
        Employee emp1 = context1.getBean("employee", Employee.class);
        System.out.println(emp1.getFirstName());

    }
}
