package hu.sprint.nn;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private String title;
    private int salary;

    public Employee() {
        this.firstName = "firstName11";
    }
}