package hu.sprint.nn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeConfiguration {

    @Bean
    public Employee emp1(){
        Employee employee = new Employee();
        employee.setFirstName("firstName1");
        employee.setLastName("lastName1");
        employee.setTitle("title1");
        employee.setLoginName("loginName1");
        employee.setPassword("password1");
        employee.setSalary(100);
        return employee;
    }

    @Bean
    public Employee emp2(){
        Employee employee = new Employee();
        employee.setFirstName("firstName2");
        employee.setLastName("lastName2");
        employee.setTitle("title2");
        employee.setLoginName("loginName2");
        employee.setPassword("password2");
        employee.setSalary(200);
        return employee;
    }

    @Bean
    public Employee emp3(){
        Employee employee = new Employee();
        employee.setFirstName("firstName3");
        employee.setLastName("lastName3");
        employee.setTitle("title3");
        employee.setLoginName("loginName3");
        employee.setPassword("password3");
        employee.setSalary(300);
        return employee;
    }

    @Bean
    public Employee emp4(){
        Employee employee = new Employee();
        employee.setFirstName("firstName4");
        employee.setLastName("lastName4");
        employee.setTitle("title4");
        employee.setLoginName("loginName4");
        employee.setPassword("password4");
        employee.setSalary(400);
        return employee;
    }

    @Bean
    public Employee emp5(){
        Employee employee = new Employee();
        employee.setFirstName("firstName5");
        employee.setLastName("lastName5");
        employee.setTitle("title5");
        employee.setLoginName("loginName5");
        employee.setPassword("password5");
        employee.setSalary(500);
        return employee;
    }

}
