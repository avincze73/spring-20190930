package hu.sprint.nn;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App3
{
    public static void main( String[] args )
    {
        ApplicationContext context1 =
                new AnnotationConfigApplicationContext(EmployeeConfiguration.class);
        Employee emp1 = context1.getBean("emp5", Employee.class);
        System.out.println(emp1.getFirstName());
    }
}
