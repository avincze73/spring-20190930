package hu.sprint.nn;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 * https://gitlab.com/avincze73/spring-20190930
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context1 =
                new ClassPathXmlApplicationContext("beans.xml");

        Employee emp1 = context1.getBean("emp1", Employee.class);
        System.out.println(emp1.getFirstName());
        System.out.println(emp1.hashCode());

        Employee emp1b = context1.getBean("emp1", Employee.class);
        System.out.println(emp1b.getFirstName());
        System.out.println(emp1b.hashCode());


        ApplicationContext context2 =
                new ClassPathXmlApplicationContext("beans.xml");
        Employee emp2 = context2.getBean("emp1", Employee.class);
        System.out.println(emp2.getFirstName());
        System.out.println(emp2.hashCode());
    }
}
