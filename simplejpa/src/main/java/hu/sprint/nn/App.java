package hu.sprint.nn;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        EntityManagerFactory factory =
                Persistence.createEntityManagerFactory("simple-jpa-pu");
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Employee employee = new Employee();
        employee.setFirstName("firstName1");
        employee.setLastName("lastName1");
        employee.setTitle("title1");
        employee.setLoginName("loginName1");
        employee.setPassword("password1");
        employee.setSalary(100);
        em.persist(employee);

        Employee employee1 = new Employee();
        employee1.setId(1);
        em.merge(employee1);


        em.getTransaction().commit();

        em.getTransaction().begin();

        em.getTransaction().commit();



//
//        Employee employee2 = new Employee();
//        employee2.setId(1);
//        em.getTransaction().commit();
//
//
//
        em.getTransaction().begin();
        Employee employee12 = em.find(Employee.class, 1);
        employee12.setFirstName("111");
        em.detach(employee12);
        em.remove(em.merge(employee12));
        em.getTransaction().commit();
//
//        em.getTransaction().begin();
//        em.merge(employee);
//        employee.setLastName("222");
//        em.getTransaction().commit();
    }
}
