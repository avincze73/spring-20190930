package hu.sprint.nn;

public class Positive implements ICondition {

    @Override
    public boolean test(int i) {
        return i > 0;
    }
}
