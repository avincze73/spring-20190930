package hu.sprint.nn;


import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.IntBinaryOperator;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

public class Accumulator {

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(accumulate(a, 0));
        //Policy as behaviour
        System.out.println(accumulate(a, 0, new Add()));
        System.out.println(accumulate(a, 1, new Multiply()));

        System.out.println(accumulate(a, 0, new Add(), new Positive()));
        System.out.println(accumulate(a, 1, new Multiply(), new Even()));


        System.out.println(accumulate(
                a,
                0,
                new IOperation() {
                    @Override
                    public int calculate(int a, int b) {
                        return a - b;
                    }
                },
                new ICondition() {
                    @Override
                    public boolean test(int i) {
                        return i < 0;
                    }
                }
        ));

        System.out.println(accumulate(
                a,
                0,
                (x,y)->x+y,
                x->x>0
        ));

        System.out.println(accumulate(
                a,
                0,
                MyOperations::myAdd,
                MyOperations::myPositive
        ));


        MyOperations myOperations = new MyOperations();
        System.out.println(accumulate(
                a,
                0,
                myOperations::myAdd2,
                myOperations::myPositive2
        ));


    }

    private static int accumulate(int[] t, int init) {
        for (int i = 0; i < t.length; i++) {
            init += t[i];
        }
        return init;
    }

    private static int accumulate(int[] t, int init, IOperation op) {
        for (int i = 0; i < t.length; i++) {
            init = op.calculate(init, t[i]);
        }
        return init;
    }

    private static int accumulate(int[] t, int init,
                                  IOperation op,
                                  ICondition cond) {
        for (int i = 0; i < t.length; i++) {
            if (cond.test(t[i])) {
                init = op.calculate(init, t[i]);
            }
        }
        return init;
    }

    private static int accumulate2(int[] t, int init,
                                   IntBinaryOperator op,
                                   IntPredicate cond) {
        for (int i = 0; i < t.length; i++) {
            if (cond.test(t[i])) {
                init = op.applyAsInt(init, t[i]);
            }
        }
        List<String> ss = new ArrayList<>();
        return init;
    }
}


class MyOperations{
    public static int myAdd(int a, int b){
        return a + b;
    }
    public static boolean myPositive(int a){
        return a > 0;
    }

    public  int myAdd2(int a, int b){
        return a + b;
    }
    public  boolean myPositive2(int a){
        return a > 0;
    }
}
