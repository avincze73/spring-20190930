package hu.sprint.nn;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FruitFactory {
    private Map<String, Supplier<Fruit>> fruitMap;

    public FruitFactory() {
        fruitMap = new HashMap<>();
        fruitMap.put("a", Apple::new);
        fruitMap.put("b", Banana::new);
    }

    public Fruit create(String s){
        return fruitMap.get(s).get();
    }

}
