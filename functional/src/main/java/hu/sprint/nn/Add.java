package hu.sprint.nn;

public class Add implements IOperation {
    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
