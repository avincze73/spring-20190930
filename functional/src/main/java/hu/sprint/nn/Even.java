package hu.sprint.nn;

public class Even implements ICondition {

    @Override
    public boolean test(int i) {
        return (i % 2) == 0;
    }
}
