package hu.sprint.nn;

public class Multiply implements IOperation {
    @Override
    public int calculate(int a, int b) {
        return a * b;
    }
}
