package hu.sprint.nn;

public interface ICondition {
    boolean test(int i);
}
