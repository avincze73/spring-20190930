package hu.sprint.nn;

public interface IOperation {
    int calculate(int a, int b);
}
